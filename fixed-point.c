/*
Each number is stored as pointer to limbs, with counts of integer and
fractional parts.

#ifdef BIG_ENDIAN

The limbs are stored in big-endian order (each limb
is stored in native-endian order):

    -(i-1) ... -2 -1 0 . 1 2 .. f

The represented value is:

$$ \sum_{n=0}^{i + f} p\left\[n\right\] b^{i - 1 - n} $$

#else

The limbs are stored in little-endian order (each limb
is stored in native-endian order):

    f .. 2 1 . 0 -1 -2 ... -(i-1)

The represented value is:

$$ \sum_{n=0}^{i + f} p\left\[n\right\] b^{n - f} $$

#endif

where $b = 2^{number of bits in a word}$.

*/

#ifdef __OPENCL_VERSION__

// OpenCL

#pragma OPENCL_EXTENSION cl_khr_fp64 : enable

#ifndef LIMBS
#error LIMBS not defined
#endif

// limb types: wordword should have twice the bits of word
typedef uint fixed_word_t;
typedef ulong fixed_wordword_t;

#define static
#define inline
#define assert(x) do{}while(0)

#else

// C99 / C11 / C++11+VLA / ...

#include <assert.h>
#include <float.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// only used for string->fixed conversion
#include <mpfr.h>

// limb types: wordword should have twice the bits of word
typedef uint32_t fixed_word_t;
typedef uint64_t fixed_wordword_t;

#define __kernel
#define __global
#define __constant const

#endif

static inline
fixed_word_t fixed_msb(const fixed_wordword_t ww)
{
  return ww >> (sizeof(fixed_word_t) * CHAR_BIT);
}

static inline
fixed_word_t fixed_lsb(const fixed_wordword_t ww)
{
  return ww;
}

static inline
fixed_wordword_t fixed_word_mul(fixed_word_t a, fixed_word_t b)
{
  return (fixed_wordword_t) a * (fixed_wordword_t) b;
}

static inline
bool fixed_lt_zero
( const fixed_word_t *a, int ai, int af
)
{
  if (ai == 0 && af == 0)
  {
    return false;
  }
#ifdef BIG_ENDIAN
  int ix = 0;
#else
  int ix = ai + af - 1;
#endif
  return !!(a[ix] & ((fixed_word_t) 1 << (sizeof(fixed_word_t) * CHAR_BIT - 1)));
}

static inline
fixed_word_t fixed_read_be
( const fixed_word_t *a, int ai, int af
, int aw
)
{
  if (ai == 0 && af == 0)
  {
    return (fixed_word_t) 0;
  }
  if (aw < -(ai - 1))
  {
    // sign extend
    if (fixed_lt_zero(a, ai, af))
    {
      return ~ (fixed_word_t) 0;
    }
    else
    {
      return   (fixed_word_t) 0;
    }
  }
  if (aw >= ai + af)
  {
    // trailing 0
    return (fixed_word_t) 0;
  }
  int ix = (ai - 1) + aw;
  return a[ix];
}

static inline
fixed_word_t fixed_read_le
( const fixed_word_t *a, int ai, int af
, int aw
)
{
  if (ai == 0 && af == 0)
  {
    return (fixed_word_t) 0;
  }
  if (aw < -(ai - 1))
  {
    // sign extend
    if (fixed_lt_zero(a, ai, af))
    {
      return ~ (fixed_word_t) 0;
    }
    else
    {
      return   (fixed_word_t) 0;
    }
  }
  if (aw >= ai + af)
  {
    // trailing 0
    return (fixed_word_t) 0;
  }
  int ix = af - aw;
  return a[ix];
}

static inline
void fixed_write_be
( fixed_word_t *a, int ai, int af
, int aw
, fixed_word_t w
)
{
  assert(-(ai - 1) <= aw);
  assert(aw <= af);
  int ix = (ai - 1) + aw;
  a[ix] = w;
}

static inline
void fixed_write_le
( fixed_word_t *a, int ai, int af
, int aw
, fixed_word_t w
)
{
  assert(-(ai - 1) <= aw);
  assert(aw <= af);
  int ix = af - aw;
  a[ix] = w;
}

#ifdef BIG_ENDIAN
#define fixed_read  fixed_read_be
#define fixed_write fixed_write_be
#else
#define fixed_read  fixed_read_le
#define fixed_write fixed_write_le
#endif

static inline
fixed_word_t fixed_to_word
( const fixed_word_t *a, int ai, int af
)
{
  return fixed_read(a, ai, af, 0);
}

double fixed_to_double
( const fixed_word_t *a, int ai, int af
)
{
  if (fixed_lt_zero(a, ai, af))
  {
    double o = 0;
    for (int aw = -(ai - 1); aw <= af; ++aw)
    {
      fixed_word_t limb = fixed_read(a, ai, af, aw);
      o += ldexp((double) ~limb, -aw * sizeof(fixed_word_t) * CHAR_BIT);
    }
    return -o;
  }
  else
  {
    double o = 0;
    for (int aw = -(ai - 1); aw <= af; ++aw)
    {
      fixed_word_t limb = fixed_read(a, ai, af, aw);
      o += ldexp((double) limb, -aw * sizeof(fixed_word_t) * CHAR_BIT);
    }
    return o;
  }
}

void fixed_zero
( fixed_word_t *a, int ai, int af
)
{
  for (int ix = 0; ix < ai + af; ++ix)
  {
    a[ix] = 0;
  }
}

void fixed_copy
(       fixed_word_t *o, int oi, int of
, const fixed_word_t *a, int ai, int af
)
{
  if (o == a && oi == ai && of == af)
  {
    return;
  }
  for (int ow = -(oi - 1); ow <= of; ++ow)
  {
    fixed_word_t w = fixed_read(a, ai, af, ow);
    fixed_write(o, oi, of, ow, w);
  }
  return;
}

void fixed_shift
(       fixed_word_t *o, int oi, int of
, const fixed_word_t *a, int ai, int af
, int b
)
{
  if (b == 0)
  {
    fixed_copy(o, oi, of, a, ai, af);
    return;
  }
  if (b > 0)
  {
    // left to right, to support o == a
    int extract = b % (sizeof(fixed_word_t) * CHAR_BIT);
    int shift = b / (sizeof(fixed_word_t) * CHAR_BIT);
    int ow = -(oi - 1);
    fixed_wordword_t w = 0;
    w <<= sizeof(fixed_word_t) * CHAR_BIT;
    w |= fixed_read(a, ai, af, ow + shift);
    for (; ow <= of; ++ow)
    {
      w <<= sizeof(fixed_word_t) * CHAR_BIT;
      w |= fixed_read(a, ai, af, ow + (shift + 1));
      fixed_write(o, oi, of, ow, fixed_msb(w << extract));
    }
    return;
  }
  if (b < 0)
  {
    // right to left, to support o == a
    b = -b;
    int extract = b % (sizeof(fixed_word_t) * CHAR_BIT);
    int shift = b / (sizeof(fixed_word_t) * CHAR_BIT);
    int ow = of;
    fixed_wordword_t w = 0;
    w >>= sizeof(fixed_word_t) * CHAR_BIT;
    w |= (fixed_wordword_t) fixed_read(a, ai, af, ow - shift)
        << (sizeof(fixed_word_t) * CHAR_BIT);
    for (; ow >= -(oi - 1); --ow)
    {
      w >>= sizeof(fixed_word_t) * CHAR_BIT;
      w |= (fixed_wordword_t) fixed_read(a, ai, af, ow - (shift + 1))
          << (sizeof(fixed_word_t) * CHAR_BIT);
      fixed_write(o, oi, of, ow, fixed_lsb(w >> extract));
    }
    return;
  }
}

fixed_word_t fixed_neg
(       fixed_word_t *o, int oi, int of
, const fixed_word_t *a, int ai, int af
)
{
  fixed_word_t carry = 1;
  for (int ow = of; ow >= -(oi - 1); --ow)
  {
    fixed_word_t in_a = fixed_read(a, ai, af, ow);
    fixed_word_t not_a = ~in_a;
    fixed_word_t a_result = not_a + carry;
    carry = a_result < not_a ? 1 : 0;
    fixed_write(o, oi, of, ow, a_result);
  }
  return carry;
}

void fixed_from_word
( fixed_word_t *a, int ai, int af
, fixed_word_t x
)
{
  fixed_zero(a, ai, af);
  fixed_write(a, ai, af, 0, x);
}

void fixed_from_double
( fixed_word_t *a, int ai, int af
, double x
)
{
  // FIXME may be inaccurate if 2 > af
  assert(FLT_RADIX == 2);
  if (x == 0)
  {
    fixed_zero(a, ai, af);
    return;
  }
  bool negate = x < 0;
  if (negate) x = -x;
  int e = 0;
  x = frexp(x, &e);
  // [0.5 .. 1), original value is x * 2^e
  for (int aw = -(ai - 1); aw <= 0; ++ aw)
  {
    fixed_write(a, ai, af, aw, 0);
  }
  for (int aw = 1; aw <= af; ++aw)
  {
    x = ldexp(x, sizeof(fixed_word_t) * CHAR_BIT);
    fixed_word_t w = floor(x);
    x -= (double) w;
    fixed_write(a, ai, af, aw, w);
    // also writes trailing 0s
  }
  fixed_shift(a, ai, af, a, ai, af, e);
  if (negate)
  {
    fixed_neg(a, ai, af, a, ai, af);
  }
  return;
}

fixed_word_t fixed_add
(       fixed_word_t *o, int oi, int of
, const fixed_word_t *a, int ai, int af
, const fixed_word_t *b, int bi, int bf
)
{
  fixed_word_t carry = 0;
  for (int ow = of; ow >= -(oi - 1); --ow)
  {
    fixed_word_t in_a = fixed_read(a, ai, af, ow);
    fixed_word_t in_b = fixed_read(b, bi, bf, ow);
    fixed_word_t a_result = in_a + carry;
    fixed_word_t a_carry = a_result < in_a ? 1 : 0;
    fixed_word_t b_result = in_b + a_result;
    fixed_word_t b_carry = b_result < in_b ? 1 : 0;
    carry = a_carry + b_carry;
    fixed_write(o, oi, of, ow, b_result);
  }
  return carry;
}

fixed_word_t fixed_sub
(       fixed_word_t *o, int oi, int of
, const fixed_word_t *a, int ai, int af
, const fixed_word_t *b, int bi, int bf
)
{
  fixed_word_t borrow = 0;
  for (int ow = of; ow >= -(oi - 1); --ow)
  {
    fixed_word_t in_a = fixed_read(a, ai, af, ow);
    fixed_word_t in_b = fixed_read(b, bi, bf, ow);
    fixed_word_t a_result = in_a - borrow;
    fixed_word_t a_borrow = borrow > in_a ? 1 : 0;
    fixed_word_t b_result = a_result - in_b;
    fixed_word_t b_borrow = in_b > a_result ? 1 : 0;
    borrow = a_borrow + b_borrow;
    fixed_write(o, oi, of, ow, b_result);
  }
  return borrow;
}

fixed_word_t fixed_mul
(       fixed_word_t *o, int oi, int of
, const fixed_word_t *a, int ai, int af
, const fixed_word_t *b, int bi, int bf
)
{
  fixed_wordword_t accum = 0;
  fixed_wordword_t carry = 0;
  for (int ow = of; ow >= -(oi - 1); --ow)
  {
    accum = fixed_lsb(carry);
    carry = fixed_msb(carry);
    if (ow == of)
    {
      // sum fixed_msb over aw + bw = ow + 1, aw <= af, bw <= bf
      for (int aw = ow + 1 - bf; aw <= af; ++aw)
      {
        int bw = ow + 1 - aw;
        fixed_word_t in_a = fixed_read(a, ai, af, aw);
        fixed_word_t in_b = fixed_read(b, bi, bf, bw);
        fixed_wordword_t ab = fixed_word_mul(in_a, in_b);
        accum += fixed_msb(ab);
      }
    }
    // sum fixed_lsb over aw + bw = ow, aw <= af, bw <= bf
    for (int aw = ow - bf; aw <= af; ++aw)
    {
      int bw = ow - aw;
      fixed_word_t in_a = fixed_read(a, ai, af, aw);
      fixed_word_t in_b = fixed_read(b, bi, bf, bw);
      fixed_wordword_t ab = fixed_word_mul(in_a, in_b);
      accum += fixed_lsb(ab);
      carry += fixed_msb(ab);
    }
    fixed_write(o, oi, of, ow, fixed_lsb(accum));
    carry += fixed_msb(accum);
  }
  return fixed_lsb(carry);
}

fixed_word_t fixed_sqr
(       fixed_word_t *o, int oi, int of
, const fixed_word_t *a, int ai, int af
)
{
  fixed_wordword_t accum = 0;
  fixed_wordword_t carry = 0;
  for (int ow = of; ow >= -(oi - 1); --ow)
  {
    accum = fixed_lsb(carry);
    carry = fixed_msb(carry);
    if (ow == of)
    {
      // sum fixed_msb over aw + bw = ow + 1, aw <= af, bw <= bf, exploiting symmetry
      for (int aw = ow + 1 - af; aw <= af; ++aw)
      {
        int bw = ow + 1 - aw;
        if (aw <= bw)
        {
          fixed_word_t in_a = fixed_read(a, ai, af, aw);
          fixed_word_t in_b = aw == bw ? in_a : fixed_read(a, ai, af, bw);
          fixed_wordword_t ab = fixed_word_mul(in_a, in_b);
          if (aw != bw)
          {
            accum += (fixed_wordword_t) fixed_msb(ab) << 1;
          }
          else
          {
            accum += fixed_msb(ab);
          }
        }
      }
    }
    // sum fixed_lsb over aw + bw = ow, aw <= af, bw <= bf, exploiting symmetry
    for (int aw = ow - af; aw <= af; ++aw)
    {
      int bw = ow - aw;
      if (aw <= bw)
      {
        fixed_word_t in_a = fixed_read(a, ai, af, aw);
        fixed_word_t in_b = aw == bw ? in_a : fixed_read(a, ai, af, bw);
        fixed_wordword_t ab = fixed_word_mul(in_a, in_b);
        if (aw != bw)
        {
          accum += (fixed_wordword_t) fixed_lsb(ab) << 1;
          carry += (fixed_wordword_t) fixed_msb(ab) << 1;
        }
        else
        {
          accum += fixed_lsb(ab);
          carry += fixed_msb(ab);
        }
      }
    }
    fixed_write(o, oi, of, ow, fixed_lsb(accum));
    carry += fixed_msb(accum);
  }
  return fixed_lsb(carry);
}

static inline
double double_sqr(double x)
{
  return x * x;
}

double mandelbrot_double_core(double cx, double cy, int maxiters)
{
  double zx = 0;
  double zy = 0;
  for (int n = 0; n < maxiters; ++n)
  {
    double zx2 = double_sqr(zx);
    double zy2 = double_sqr(zy);
    double z2 = zx2 + zy2;
    if (z2 >= 256)
    {
      return n + 1 - log2(0.5 * log(z2));
    }
    double zxy = zx * zy;
    zx = zx2 - zy2 + cx;
    zy = 2 * zxy + cy;
  }
  return 1.0 / 0.0; // infinity for interior
}

/*
local memory usage in bytes:

    ((i + f) * 5 + 3) * sizeof(word)

*/
double mandelbrot_fixed_core
( const fixed_word_t *cx
, const fixed_word_t *cy
, int maxiters
#ifdef __OPENCL_VERSION__
// no VLA, use global #define
#else
, int LIMBS
#endif
)
{
#define i 1
#define f LIMBS
#define w (i + f)
  fixed_word_t zx[w], zy[w], zx2[w], zy2[w], zxy[w], z2[1+2];
  fixed_zero(zx, i, f);
  fixed_zero(zy, i, f);
  for (int n = 0; n < maxiters; ++n)
  {
    fixed_sqr(zx2, i, f, zx, i, f);
    fixed_sqr(zy2, i, f, zy, i, f);
    fixed_add(z2, 1, 2, zx2, i, f, zy2, i, f);
    if (fixed_to_word(z2, 1, 2) >= 256)
    {
      double d = fixed_to_double(z2, 1, 2);
      return n + 1 - log2(0.5 * log(d));
    }
    fixed_mul(zxy, i, f, zx, i, f, zy, i, f);
    fixed_sub(zx, i, f, zx2, i, f, zy2, i, f);
    fixed_add(zx, i, f, zx, i, f, cx, i, f);
    fixed_shift(zy, i, f, zxy, i, f, 1);
    fixed_add(zy, i, f, zy, i, f, cy, i, f);
  }
  return 1.0 / 0.0; // infinity for interior
#undef i
#undef f
#undef w
}

fixed_word_t hash_burtle_9(fixed_word_t a)
{
  // FIXME assumes 32bit word
  a = (a+0x7ed55d16u) + (a<<12u);
  a = (a^0xc761c23cu) ^ (a>>19u);
  a = (a+0x165667b1u) + (a<<5u);
  a = (a+0xd3a2646cu) ^ (a<<9u);
  a = (a+0xfd7046c5u) + (a<<3u);
  a = (a^0xb55a4f09u) ^ (a>>16u);
  return a;
}

double uniform01(int seed)
{
  fixed_word_t r = hash_burtle_9(seed);
  return (double)(r) / ((double)(fixed_word_t)(-1) + 1);
}

__kernel
void mandelbrot_double
( __global unsigned char* output
, int subframe
, int height
, int width
, double cx0
, double cy0
, double r0
, int maxiters
)
{
#ifdef __OPENCL_VERSION__
 int y = get_global_id(0);
 int x = get_global_id(1);
#else
 #pragma omp parallel for
 for (int y = 0; y < height; ++y)
 for (int x = 0; x < width; ++x)
#endif
 {
  int k = (y * width + x) * 3;
  int seed = subframe * width * height + y * width + x;
  double dx = 2 * r0 * ((x + uniform01(2 * seed + 0)) / width  - 0.5) * width / height;
  double dy = 2 * r0 * ((height - (y + uniform01(2 * seed + 1))) / height - 0.5);
  double mu = mandelbrot_double_core(cx0 + dx, cy0 + dy, maxiters);
  if (mu == -1.0 / 0.0)
  {
    output[k + 0] = 0;
    output[k + 1] = 0;
    output[k + 2] = 0;
  }
  else
  {
    output[k + 0] = 255 * (0.5 - 0.5 * cos(mu / 1  ));
    output[k + 1] = 255 * (0.5 - 0.5 * cos(mu / 10 ));
    output[k + 2] = 255 * (0.5 - 0.5 * cos(mu / 100));
  }
 }
}

__kernel
void mandelbrot_fixed
( __global unsigned char* output
, int subframe
, int height
, int width
, __constant fixed_word_t *cx0
, __constant fixed_word_t *cy0
, double r0
, int maxiters
#ifdef __OPENCL_VERSION__
// no VLA, use global #define
#else
, int LIMBS
#endif
)
{
#ifdef __OPENCL_VERSION__
 int y = get_global_id(0);
 int x = get_global_id(1);
#else
 #pragma omp parallel for
 for (int y = 0; y < height; ++y)
 for (int x = 0; x < width; ++x)
#endif
 {
  int k = (y * width + x) * 3;
  int seed = subframe * width * height + y * width + x;
  double dx = 2 * r0 * ((x + uniform01(2 * seed + 0)) / width  - 0.5) * width / height;
  double dy = 2 * r0 * ((height - (y + uniform01(2 * seed + 1))) / height - 0.5);
  fixed_word_t cx[1+LIMBS], cy[1+LIMBS];
  fixed_from_double(cx, 1, LIMBS, dx);
  fixed_from_double(cy, 1, LIMBS, dy);
  {
    fixed_word_t t[1+LIMBS];
    for (int i = 0; i <= LIMBS; ++i)
    {
#ifdef SWAP_ENDIAN
      t[i] = cx0[LIMBS - i];
#else
      t[i] = cx0[i];
#endif
    }
    fixed_add(cx, 1, LIMBS, cx, 1, LIMBS, t, 1, LIMBS);
    for (int i = 0; i <= LIMBS; ++i)
    {
#ifdef SWAP_ENDIAN
      t[i] = cy0[LIMBS - i];
#else
      t[i] = cy0[i];
#endif
    }
    fixed_add(cy, 1, LIMBS, cy, 1, LIMBS, t, 1, LIMBS);
  }
  double mu = mandelbrot_fixed_core
    ( cx
    , cy
    , maxiters
#ifdef __OPENCL_VERSION__
    // no VLA, use global #define
#else
    , LIMBS
#endif
    );
  if (mu == -1.0 / 0.0)
  {
    output[k + 0] = 0;
    output[k + 1] = 0;
    output[k + 2] = 0;
  }
  else
  {
    output[k + 0] = 255 * (0.5 - 0.5 * cos(mu / 1  ));
    output[k + 1] = 255 * (0.5 - 0.5 * cos(mu / 10 ));
    output[k + 2] = 255 * (0.5 - 0.5 * cos(mu / 100));
  }
 }
}

#ifndef __OPENCL_VERSION__

void fixed_dump
( FILE *f
, const fixed_word_t *a, int ai, int af
)
{
  for (int aw = -(ai - 1); aw <= 0; ++aw)
  {
    fprintf(f, "%08x", fixed_read(a, ai, af, aw));
    if (aw < 0) fputc(' ', f);
  }
  fputc('.', f);
  for (int aw = 1; aw <= af; ++aw)
  {
    fprintf(f, "%08x", fixed_read(a, ai, af, aw));
    if (aw < af) fputc(' ', f);
  }
}

void fixed_from_mpfr
( fixed_word_t *a, int ai, int af
, const mpfr_t x
)
{
  fixed_word_t b[ai + af];
  mpfr_t y;
  mpfr_init2(y, mpfr_get_prec(x));
  mpfr_set(y, x, MPFR_RNDN);
  fixed_zero(a, ai, af);
  double limb;
  do
  {
    limb = mpfr_get_d(y, MPFR_RNDN);
    mpfr_sub_d(y, y, limb, MPFR_RNDN);
    fixed_from_double(b, ai, af, limb);
    fixed_add(a, ai, af, a, ai, af, b, ai, af);
  } while (limb != 0);
  mpfr_clear(y);
}

void fixed_from_string
( fixed_word_t *a, int ai, int af
, const char *x
)
{
  mpfr_t y;
  mpfr_init2(y, (ai + af + 1) * sizeof(fixed_word_t) * CHAR_BIT);
  mpfr_set_str(y, x, 10, MPFR_RNDN);
  fixed_from_mpfr(a, ai, af, y);
  mpfr_clear(y);
}

#endif
