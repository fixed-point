all: benchmark-be benchmark-le

benchmark-be: benchmark.cc fixed-point.c fixed-point.c.h
	g++ -std=c++11 -pedantic -Wall -Wextra -Wno-vla -O3 -march=native -fopenmp -o $@ benchmark.cc -lOpenCL -lmpfr -lm -DBIG_ENDIAN=1

benchmark-le: benchmark.cc fixed-point.c fixed-point.c.h
	g++ -std=c++11 -pedantic -Wall -Wextra -Wno-vla -O3 -march=native -fopenmp -o $@ benchmark.cc -lOpenCL -lmpfr -lm -UBIG_ENDIAN

%.c.h: %.c
	xxd -i < $< > $@
