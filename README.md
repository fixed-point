# fixed-point

A simple fixed-point implementation in C99 / C11 / C++11+VLA / OpenCL,
with a Mandelbrot set example.

## benchmark

This location can go to 1e-90 or so, need to increase iterations to
200000 for that.

    make
    export re=0.30080610945091250101274463846430580557504119646581772500839528915499621621508047696387992688316314968045302725130006488
    export im=0.02014938385452366595450795517742423251484175812702036451597574000830171696790114655811145694242397255854670284511000004
    export zoom=1e-15
    export iterations=1000
    for host_endian in le be ; do
    for use_fixed in 0 1 ; do
    for use_cl in 0 1 ; do
    for device_bigendian in $(seq 0 $use_cl) ; do
      time ./benchmark-$host_endian 1920 1080 "$re" "$im" "$zoom" "$iterations" "$use_fixed" "$use_cl" "$use_bigendian" > "${host_endian}-${use_fixed}-${use_cl}-${device_bigendian}.ppm"
    done ; done ; done ; done

## legal

Copyright (c) 2019 Claude Heiland-Allen <claude@mathr.co.uk>

released under GNU Affero General Public License
<https://www.gnu.org/licenses/agpl-3.0.html>

## references

- OpenCL C++ driver code example simplified from `mandelbrot_cl.cpp` from
<http://distrustsimplicity.net/articles/mandelbrot-speed-comparison/>
