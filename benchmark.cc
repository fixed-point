#include <iostream>

#define CL_TARGET_OPENCL_VERSION 120
#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>

#include "fixed-point.c"
const char fixed_point_cl[] = {
#include "fixed-point.c.h"
, 0
};

int main(int argc, char** argv)
{
  if (argc < 10)
  {
    std::fprintf
      ( stderr
      , "usage: %s width height cx cy r maxiters use_fixed use_cl use_bigendian > out.ppm\n"
      , argv[0]
      );
    return 1;
  }
  int subframe = 1;
  int width = atoi(argv[1]);
  int height = atoi(argv[2]);
  double r0 = atof(argv[5]);
  int prec = -log2(2 * r0 / height);
  int b = sizeof(fixed_word_t) * CHAR_BIT;
  int LIMBS = (prec + 8 + b - 1) / b;
  fixed_word_t cx0[1 + LIMBS], cy0[1 + LIMBS];
  fixed_from_string(cx0, 1, LIMBS, argv[3]);
  fixed_from_string(cy0, 1, LIMBS, argv[4]);
  int maxiters = atoi(argv[6]);
  bool use_fixed = atoi(argv[7]);
  bool use_cl = atoi(argv[8]);
  bool use_bigendian = atoi(argv[9]);
  if (use_fixed)
  {
    std::cerr << "fixed format Q1." << LIMBS << std::endl;
  }
  size_t bytes = (size_t) height * width * 3;
  unsigned char* results = new unsigned char[bytes];

  if (use_cl)
  {
    // OpenCL renderers
    try {
      std::vector<cl::Platform> platforms;
      cl::Platform::get(&platforms);
      std::vector<cl::Device> platformDevices;
      platforms[0].getDevices(CL_DEVICE_TYPE_GPU, &platformDevices);
      cl::Context context(platformDevices);
      auto contextDevices = context.getInfo<CL_CONTEXT_DEVICES>();
      auto device = contextDevices[0];
      std::cerr << "Using " << device.getInfo<CL_DEVICE_NAME>() << std::endl;
      cl::Program::Sources source
        ( 1
        , std::make_pair(fixed_point_cl, sizeof(fixed_point_cl) - 1)
        );
      cl::Program program(context, source);
      try {
        bool device_big_endian = use_bigendian;
#ifdef BIG_ENDIAN
        bool host_big_endian = true;
#else
        bool host_big_endian = false;
#endif
        char options[100];
        std::snprintf
          ( options, 100
          , "-D LIMBS=%d%s%s"
          , LIMBS
          , device_big_endian ? " -D BIG_ENDIAN=1" : ""
          , device_big_endian != host_big_endian ? " -D SWAP_ENDIAN=1" : ""
          );
        program.build(contextDevices, options);
      } catch (cl::Error &e) {
        auto buildlog = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device);
        std::cerr << buildlog << std::endl;
        return 1;
      }
      cl::CommandQueue queue(context, device, CL_QUEUE_PROFILING_ENABLE);
      cl_int err = CL_SUCCESS;
      cl::Buffer output(context, CL_MEM_WRITE_ONLY, bytes, nullptr, &err);
      cl::NDRange offset(0, 0);
      cl::NDRange global_size(height, width);
      if (use_fixed)
      {
        cl::Buffer cx0_buffer
          ( context
          , CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR
          , (1 + LIMBS) * sizeof(fixed_word_t)
          , cx0
          , &err
          );
        cl::Buffer cy0_buffer
          ( context
          , CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR
          , (1 + LIMBS) * sizeof(fixed_word_t)
          , cy0
          , &err
          );
        cl::Kernel mandelbrot(program, "mandelbrot_fixed");
        mandelbrot.setArg(0, output);
        mandelbrot.setArg(1, subframe);
        mandelbrot.setArg(2, height);
        mandelbrot.setArg(3, width);
        mandelbrot.setArg(4, cx0_buffer);
        mandelbrot.setArg(5, cy0_buffer);
        mandelbrot.setArg(6, r0);
        mandelbrot.setArg(7, maxiters);
        queue.enqueueNDRangeKernel(mandelbrot, offset, global_size);
      }
      else
      {
        double cx0_double = fixed_to_double(cx0, 1, LIMBS);
        double cy0_double = fixed_to_double(cy0, 1, LIMBS);
        cl::Kernel mandelbrot(program, "mandelbrot_double");
        mandelbrot.setArg(0, output);
        mandelbrot.setArg(1, subframe);
        mandelbrot.setArg(2, height);
        mandelbrot.setArg(3, width);
        mandelbrot.setArg(4, cx0_double);
        mandelbrot.setArg(5, cy0_double);
        mandelbrot.setArg(6, r0);
        mandelbrot.setArg(7, maxiters);
        queue.enqueueNDRangeKernel(mandelbrot, offset, global_size);
      }
      queue.enqueueBarrierWithWaitList();
      cl::Event readDoneEvent;
      queue.enqueueReadBuffer
        ( output
        , CL_FALSE
        , 0
        , bytes
        , results
        , nullptr
        , &readDoneEvent
        );
      std::vector<cl::Event> readWaitList;
      readWaitList.push_back(readDoneEvent);
      cl::Event::waitForEvents(readWaitList);
    } catch (cl::Error &e) {
      std::cerr << e.what() << ": error code " << e.err() << std::endl;
      return 1;
    }
  }
  else
  {
    // CPU renderers
    if (use_fixed)
    {
      mandelbrot_fixed
        ( results
        , subframe
        , height
        , width
        , cx0
        , cy0
        , r0
        , maxiters
        , LIMBS
        );
    }
    else
    {
      double cx = fixed_to_double(cx0, 1, LIMBS);
      double cy = fixed_to_double(cy0, 1, LIMBS);
      mandelbrot_double
        ( results
        , subframe
        , height
        , width
        , cx
        , cy
        , r0
        , maxiters
        );
    }
  }
  // output image
  std::fprintf(stdout, "P6\n%d %d\n# cx = ", width, height);
  fixed_dump(stdout, cx0, 1, LIMBS);
  std::fprintf(stdout, "\n# cy = ");
  fixed_dump(stdout, cy0, 1, LIMBS);
  std::fprintf
    ( stdout
    , "\n# r = %.18e\n# maxiters = %d\n# use_fixed = %d\n# use_cl = %d\n255\n"
    , r0
    , maxiters
    , use_fixed
    , use_cl
    );
  std::fwrite(results, bytes, 1, stdout);
  return 0;
}
